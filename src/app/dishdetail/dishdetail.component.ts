import { Component, OnInit, ViewChild, Inject} from '@angular/core';
import { Dish } from '../shared/dish';

import { Params, ActivatedRoute} from '@angular/router';
import { Location } from '@angular/common';

// services
import { DishService } from '../services/dish.service';

import { switchMap } from 'rxjs/operators';

import { FormBuilder, FormGroup, Validators, ControlContainer} from '@angular/forms';
import { Commentfeedback } from '../shared/commentsFeedback';
import { Comment } from '../shared/comment';
import { visibility, flyInOut, expand } from '../animations/app.animation';



@Component({
  selector: 'app-dishdetail',
  templateUrl: './dishdetail.component.html',
  styleUrls: ['./dishdetail.component.scss'],
      // tslint:disable-next-line: no-host-metadata-property
      host: {
        '[@flyInOut]': 'true',
        style: 'display: block;'
      },
  animations: [
    expand(),
    flyInOut(),
    visibility()
  ]
})
export class DishdetailComponent implements OnInit {


dish: Dish;
dishIds: string[];
visibility = 'shown';
errMess: string;
dishcopy: Dish;
prev: string;
next: string;
feedbackForm: FormGroup;
comment: Commentfeedback;
@ViewChild('fform') feedbackFormDirective: any;
formErrors = {
  author: '',
  comment: '',
 };

 validationMessages = {
  author: {
    required:  'First Name is required.',
    minlength: 'First Name must be at least 2 characters long.',
    maxlength: 'FirstName cannot be more than 25 characters long.'
  },
  comment: {
     required:  'Comment is required.',
  }
};


  constructor(private dishService: DishService,
              private route: ActivatedRoute,
              private location: Location, private fb: FormBuilder,
              @Inject('BaseURL') public BaseURL: any
              ) {}

  ngOnInit(): void {
    this.createForm();
    // const id = this.route.snapshot.params['id'];
    this.dishService.getDishIds()
    .subscribe(dishIds => this.dishIds = dishIds);
    this.route.params.pipe(switchMap((params: Params) => {this.visibility = 'hidden'; return  this.dishService.getDish(params.id); }))
   .subscribe(dish => { this.dish = dish; this.dishcopy = dish; this.setPrevNext(dish.id); this.visibility = 'shown'; },
        errmess => this.errMess = (errmess as any));
  }

  createForm(): any {
    this.feedbackForm = this.fb.group({
      author: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(25)]],
      rating: 5,
      comment: ['', [Validators.required]],
    });
    this.feedbackForm.valueChanges
      .subscribe(data => this.onValueChanged(data));

    this.onValueChanged(); // reset validation messagess now
  }

  onValueChanged(data?: any): any {
    if (!this.feedbackForm) { return; }
    const form = this.feedbackForm;
    for (const field in this.formErrors) {
      if (this.formErrors.hasOwnProperty(field)) {
        // clear previous error message (if any)
        this.formErrors[field] = '';
        const control = form.get(field);
        if (control && control.dirty && !control.valid) {
          const messages = this.validationMessages[field];
          for (const key in control.errors) {
            if (control.errors.hasOwnProperty(key)) {
              this.formErrors[field] += messages[key] + ' ';
            }
          }
        }
      }
    }
  }

  setPrevNext(dishId: string): any{
    const index = this.dishIds.indexOf(dishId);
    this.prev = this.dishIds[(this.dishIds.length + index - 1) % this.dishIds.length];
    this.next = this.dishIds[(this.dishIds.length + index + 1) % this.dishIds.length];
  }

  goBack(): void {
    this.location.back();
  }


formatLabel(value: number): any {
  if (value >= 6) {
    return Math.round(value / 6) + 'k';
  }

  return value;
}



onSubmit(): any {
  this.comment = this.feedbackForm.value;
  this.comment.date = new Date().toISOString();
  console.log(this.comment);
  this.dishcopy.comments.push(this.comment);
  this.dishService.putDish(this.dishcopy)
  .subscribe(dish => {
    this.dish = dish; this.dishcopy = dish;
  },
  errmess => {this.dish = null; this.dishcopy = null; this.errMess = (errmess as any); });
  this.feedbackForm.reset({
    author: '',
    rating: 5,
    comment: '',
  });
  this.feedbackFormDirective.resetForm();
}

}
