import { Component, OnInit, ViewChild, Inject  } from '@angular/core';
import { FormBuilder, FormGroup, Validators, ControlContainer } from '@angular/forms';
import { Feedback, ContactType } from '../shared/feedback';
import { flyInOut, visibility, expand } from '../animations/app.animation';
import { FeedbackService } from '../services/feedback.service';
import { switchMap } from 'rxjs/operators';
import { Location } from '@angular/common';
import { Params, ActivatedRoute} from '@angular/router';
import { FeedbackComments } from '../shared/feedbacks';







@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss'],
    // tslint:disable-next-line: no-host-metadata-property
    host: {
      '[@flyInOut]': 'true',
      style: 'display: block;'
    },
    animations: [
      flyInOut(),
      visibility(),
      expand()
    ]
})
export class ContactComponent implements OnInit {



  errMess: string;
  feedbackIds: string[];
  visibility = 'shown';
  feedbackForm: FormGroup;
  feedback: Feedback;
  feedbackcopy: Feedback;
  feedbackcomment: FeedbackComments;
  contactType = ContactType;
  form = 'Display';
  @ViewChild('fform') feedbackFormDirective: any;

  formErrors = {
   firstname: '',
    lastname: '',
    telnum: '',
     email: ''
  };

  validationMessages = {
    firstname: {
      required:  'First Name is required.',
      minlength: 'First Name must be at least 2 characters long.',
      maxlength: 'FirstName cannot be more than 25 characters long.'
    },
      lastname: {
       required:  'Last Name is required.',
       minlength: 'Last Name must be at least 2 characters long.',
       maxlength: 'Last Name cannot be more than 25 characters long.'
    },
      telnum: {
      required: 'Tel. number is required.',
      pattern:  'Tel. number must contain only numbers.'
    },
     email: {
      required: 'Email is required.',
      email: 'Email not in valid format.'
    },
  };

  constructor(private fb: FormBuilder, private location: Location,
              private feedbackService: FeedbackService,
              private route: ActivatedRoute,
              @Inject('BaseURL') public BaseURL: any) {
   }

  ngOnInit(): void {
     this.createForm();

     this.feedbackService.getFeedbackIds()
     .subscribe(feedbackIds => this.feedbackIds = feedbackIds);
     this.route.params.pipe(switchMap((params: Params) => {this.visibility = 'hidden';
                                                           return  this.feedbackService.getFeedback(params.id); }))
    .subscribe(feedback => { this.feedback = feedback; this.feedbackcopy = feedback;
                             this.visibility = 'shown'; },
         errmess => this.errMess = (errmess as any));
  }

  createForm(): any {
    this.feedbackForm = this.fb.group({
      firstname: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(25)]],
      lastname: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(25)]],
      telnum: [0, [Validators.required, Validators.pattern]],
      email: ['', [Validators.required, Validators.email]],
      agree: false,
      contacttype: 'None',
      message: ''
    });
    this.feedbackForm.valueChanges
      .subscribe(data => this.onValueChanged(data));

    this.onValueChanged(); // reset validation messagess now
  }


  onValueChanged(data?: any): any {
    if (!this.feedbackForm) { return; }
    const form = this.feedbackForm;
    for (const field in this.formErrors) {
      if (this.formErrors.hasOwnProperty(field)) {
        // clear previous error message (if any)
        this.formErrors[field] = '';
        const control = form.get(field);
        if (control && control.dirty && !control.valid) {
          const messages = this.validationMessages[field];
          for (const key in control.errors) {
            if (control.errors.hasOwnProperty(key)) {
              this.formErrors[field] += messages[key] + ' ';
            }
          }
        }
      }
    }
  }

  onSubmit(): any {
    this.form = null;
    this.feedbackcopy = this.feedbackForm.value;
    console.log(this.feedbackcomment);
    this.feedbackService.putFeedback(this.feedbackcopy)
    .subscribe(feedback => {
      this.feedback = feedback; this.feedbackcopy = feedback;
      setTimeout(() => this.DisplayFormu(), 5000);
    },
    errmess => {this.feedback = null; this.feedbackcopy = null; this.errMess = (errmess as any); });
    this.feedbackForm.reset({
                  firstname: '',
                  lastname: '',
                  telnum: 0,
                  email: '',
                  agree: false,
                  contacttype: 'None',
                  message: ''
                });
    this.feedbackFormDirective.resetForm();
  }

  // tslint:disable-next-line: typedef
  DisplayFormu() {
    this.form = 'display';
    this.feedbackcopy =  null;
    this.feedback = null;
    return 1;
  }

}
