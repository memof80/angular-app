import { Component, OnInit, Inject } from '@angular/core';
import { Dish } from '../shared/dish';
import { DishService } from '../services/dish.service';
import { flyInOut, expand } from '../animations/app.animation';




@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss'],
  // tslint:disable-next-line: no-host-metadata-property
  host: {
    '[@flyInOut]': 'true',
    style: 'display: block;'
  },
  animations: [
    expand(),
    flyInOut()
  ]
})
export class MenuComponent implements OnInit {

  dishes: Dish[];
  errMess: string;

  // selectedDish: Dish;

  constructor(private dishService: DishService,
              @Inject('BaseURL') public BaseURL: any)  { }

  ngOnInit(): void {
     this.dishService.getDishes()
    .subscribe((dishes) => this.dishes  = dishes,
      errmess => this.errMess = (errmess as any)); // => <any>errmess
  }

    // onSelect(dish: Dish): any {
    //   this.selectedDish = dish;
    // }
}
