import { FeedbackComments} from './feedbacks';
export class Feedback {
    id: string;
    firstname: string;
    lastname: string;
    telnum: number;
    email: string;
    agree: boolean;
    contacttype: string;
    message: string;
    feedbacks: FeedbackComments[];
}


export const ContactType = ['None', 'Tel', 'Email'];
