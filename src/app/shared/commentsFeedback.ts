export class Commentfeedback {
    author: string;
    rating: number;
    comment: string;
    date: string;
}
