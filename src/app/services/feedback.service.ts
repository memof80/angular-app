import { Injectable } from '@angular/core';
import { Feedback } from '../shared/feedback';

import { of, Observable } from 'rxjs';
import { delay, map, catchError } from 'rxjs/operators';

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { baseURL } from '../shared/baseurl';
import { ProcessHTTPMsgService } from './process-httpmsg.service';

@Injectable({
  providedIn: 'root'
})
export class FeedbackService {

  constructor(private http: HttpClient, private processHTTPMsgService: ProcessHTTPMsgService) { }

  getFeedbacks(): Observable<Feedback[]> {
    return this.http.get<Feedback[]>(baseURL + 'feedback')
    .pipe(catchError(this.processHTTPMsgService.handleError));
  }

  getFeedback(id: string): Observable<Feedback> {
    return this.http.get<Feedback>(baseURL + 'feedback/')
    .pipe(catchError(this.processHTTPMsgService.handleError));
  }

  getFeaturedFeedback(): Observable<Feedback>  {
    return  this.http.get<Feedback[]>(baseURL + 'feedback?featured=true').pipe(map(feedbacks => feedbacks[0]))
    .pipe(catchError(this.processHTTPMsgService.handleError));
  }

  getFeedbackIds(): Observable<string[] | any> {
    return this.getFeedbacks().pipe(map(feedbacks => feedbacks.map( feedback => feedback.id)))
    .pipe(catchError(error => error));
  }

  putFeedback(feedback: Feedback): Observable<Feedback> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    return this.http.post<Feedback>(baseURL + 'feedback/', feedback, httpOptions)
    .pipe(catchError(this.processHTTPMsgService.handleError));
  }

}
