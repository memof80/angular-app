import { HighlightDirective } from './highlight.directive';

describe('HighlightDirective', () => {
  it('should create an instance', () => {
    let elRefMock = {
      nativeElement: document.createElement('div')
    };
    const renderer2Mock = jasmine.createSpyObj('renderer2Mock', [
      'destroy',
      'createElement',
      'createComment',
      'createText',
      'destroyNode',
      'appendChild',
      'insertBefore',
      'removeChild',
      'selectRootElement',
      'parentNode',
      'nextSibling',
      'setAttribute',
      'removeAttribute',
      'addClass',
      'removeClass',
      'setStyle',
      'removeStyle',
      'setProperty',
      'setValue',
      'listen'
    ]);
    const rootRendererMock =  {
      renderComponent: () => {
          return renderer2Mock;
      }
    };
    const directive =  new HighlightDirective(elRefMock, renderer2Mock);
    expect(directive).toBeTruthy();
  });
});
